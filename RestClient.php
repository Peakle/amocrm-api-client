<?php

namespace peakle\amoCrm;

use \Exception;
use \DateTime;
use \DateTimeZone;

/**
 * @author <denislimarev@gmail.com> peakle
 * Class Client
 */
class RestClient
{

    protected $path = "./credentials/";
    protected $user;
    protected $api_key;
    protected $subdomain;

    const Unauthorized = "Auth error";
    const Forbidden = "forbidden";
    const MovedPermanently = "Moved permanently";
    const BadRequest = "Bad request";
    const NotFound = "Not found";
    const BadGateway = "Bad gateway";
    const InternalServerError = "Internal server error";
    const ServiceUnavailable = "Service unavailable";

    public function __construct($user, $api_key, $account)
    {
        $this->user = $user;
        $this->api_key = $api_key;
        $this->subdomain = $account;
    }

    /**
     * Update status of leads array
     *
     * @param array $data
     * @return array|null
     * @throws Exception
     */
    public function updateLeads(array &$data): ?array
    {
        $leads ['update'] = [];

        $date = new DateTime('now', new DateTimeZone('Europe/Moscow'));
        $date_stamp = $date->getTimestamp();

        foreach ($data as $key => $lead_params) {

            $leads ['update'][$key] = [
                'id' => $lead_params['id'],
                'updated_at' => $date_stamp,
                'status_id' => $lead_params['status_id'],
            ];

        }

        $update_leads = $this->amoAddChange($leads, "/api/v2/leads");

        if (isset($update_leads[0])) {
            return $update_leads;
        }

        return null;
    }

    /**
     * Add array of leads
     *
     * @param array $data
     * @return array|null
     * @throws Exception
     */
    public function addLeads(array &$data): ?array
    {
        $leads ['add'] = [];

        foreach ($data as $key => $lead_params) {

            $leads ['add'][$key] = [
                'name' => $lead_params['name'],
                'status_id' => $lead_params['status_id'],
            ];

            if (isset($lead_params['responsible_user_id'])) {
                $leads['add'][$key]['responsible_user_id'] = $lead_params['responsible_user_id'];
            }

            if (isset($lead_params['contacts_id'])) {
                foreach ($lead_params['contacts_id'] as $contact_id) {
                    $leads['add'][$key]['contacts_id'][] = $contact_id;
                }
            }
        }

        $new_leads = $this->amoAddChange($leads, "/api/v2/leads");

        if (isset($new_leads[0])) {
            return $new_leads;
        }

        return null;
    }

    /**
     * Add array of contacts
     *
     * @param array $data
     * @return array|null
     * @throws Exception
     */
    public function addContacts(array &$data): ?array
    {
        $contacts ['add'] = [];

        foreach ($data as $key => $contact_params) {

            $contacts ['add'][$key] = [
                'name' => $contact_params['name'],
                'custom_fields' => []
            ];

            if (isset($contact_params['phones'])) {

                foreach ($contact_params['phones'] as $phone) {
                    $contacts['add'][$key]['custom_fields'][] = [
                        'id' => $phone['phone_field_id'],
                        "values" => [
                            [
                                'value' => $phone['phone'],
                                'enum' => $phone['phone_enum']
                            ]
                        ],
                    ];
                }
            }

            if (isset($contact_params['emails'])) {

                foreach ($contact_params['emails'] as $email) {
                    $contacts['add'][$key]['custom_fields'][] = [
                        "id" => $email['email_field_id'],
                        "values" => [
                            [
                                'value' => $email['email'],
                                'enum' => $email['email_enum']
                            ]
                        ]
                    ];
                }
            }
        }

        $new_contacts = $this->amoAddChange($contacts, "/api/v2/contacts");

        if (isset($new_contacts['0'])) {
            return $new_contacts;
        }

        return null;
    }


    /**
     * Process add and update queries
     *
     * @param array $array_data
     * @param string $scope
     * @return array|null
     * @throws Exception
     */
    public function amoAddChange(array &$array_data, string $scope): ?array
    {
        $data_json = json_encode($array_data);

        $opts = [
            0 => [
                'option' => CURLOPT_CUSTOMREQUEST,
                'value' => 'POST'
            ],
            1 => [
                'option' => CURLOPT_POSTFIELDS,
                'value' => $data_json
            ],
            2 => [
                'option' => CURLOPT_USERAGENT,
                'value' => 'amoCRM-API-client/1.0'
            ],
            3 => [
                'option' => CURLOPT_HTTPHEADER,
                'value' => ['Content-Type: application/json']
            ],
        ];

        $url = "https://" . $this->subdomain . ".amocrm.ru{$scope}";

        $response = $this->sendRequest($opts, $url, 0);

        $response = $response['_embedded']['items'];

        if (isset($response[0])) {
            return $response;
        }

        return null;
    }

    /**
     * Find contact or lead by email, name or id
     *
     * @param string $value - search string
     * @param string $scope - scope of search , for example "leads?query"
     * @return array|null
     * @throws Exception
     */
    public function amoFindBy(string $scope, string $value): ?array
    {
        $opts = [
            0 => [
                'option' => CURLOPT_USERAGENT,
                'value' => 'amoCRM-API-client/1.0'
            ],
            1 => [
                'option' => CURLOPT_HTTPHEADER,
                'value' => ['Accept: application/json']
            ],
        ];

        $url = "https://" . $this->subdomain . ".amocrm.ru/api/v2/{$scope}=" . $value;

        $response = $this->sendRequest($opts, $url, 0);

        $response = $response['_embedded']['items'];

        if (isset($response[0])) {
            return $response;
        }

        return null;
    }


    /**
     * Auth server on amoCrm
     *
     * @param int $try_count - auth tries count
     * @return bool|null
     * @throws Exception
     */
    public function authServerAmo(int $try_count = 0): ?bool
    {
        $url = "https://" . $this->subdomain . ".amocrm.ru/private/api/auth.php?type=json";

        $user = json_encode([
            'USER_LOGIN' => $this->user,
            'USER_HASH' => $this->api_key
        ]);

        $opts = [
            0 => [
                'option' => CURLOPT_CUSTOMREQUEST,
                'value' => 'POST'
            ],
            1 => [
                'option' => CURLOPT_POSTFIELDS,
                'value' => $user
            ],
            2 => [
                'option' => CURLOPT_USERAGENT,
                'value' => 'amoCRM-API-client/1.0'
            ],
            3 => [
                'option' => CURLOPT_HTTPHEADER,
                'value' => ['Content-Type: application/json']
            ],
            4 => [
                'option' => CURLOPT_COOKIEJAR,
                'value' => "{$this->path}cookie.txt"
            ]
        ];

        $res = $this->sendRequest($opts, $url, $try_count);

        if (isset($res['response']['auth'])) {
            return true;
        }

        return null;
    }


    /**
     * Send requests to amoCrm
     *
     * @param array $opts
     * @param string $url
     * @param int $try_count - auth tries count
     * @return mixed
     * @throws Exception
     */
    public function sendRequest(array $opts, string $url, int $try_count = 0)
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, "{$this->path}cookie.txt");
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

        foreach ($opts as $opt) {
            curl_setopt($curl, $opt['option'], $opt['value']);
        }

        $out = curl_exec($curl);

        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        $code = (int)$code;

        if ($code != 200 && $code != 204) {

            //if got auth error try one more time
            if ($code == 401 && $try_count == 0) {
                $this->authServerAmo(1);
                return $this->sendRequest($opts, $url, 1);
            } else {
                $this->process_error($code);
            }
        }


        $response = json_decode($out, true);

        return $response;
    }


    /**
     * @param int $code
     * @throws Exception
     */
    protected function process_error(int $code)
    {
        switch ($code) {
            case 301:
                throw new Exception(self::MovedPermanently);
                break;

            case 400:
                throw new Exception(self::BadRequest);
                break;

            case 401:
                throw new Exception(self::Unauthorized);
                break;

            case 403:
                throw new Exception(self::Forbidden);
                break;

            case 404:
                throw new Exception(self::NotFound);
                break;

            case 500:
                throw new Exception(self::InternalServerError);
                break;

            case 502:
                throw new Exception(self::BadGateway);
                break;

            case 503:
                throw new Exception(self::ServiceUnavailable);
                break;
        }
    }

}
