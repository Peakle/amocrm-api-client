<?php

require_once('RestClient.php');

use peakle\amoCrm\RestClient as Client;

class example
{
    public $service;

    public function __construct()
    {
        $this->service = new Client('amo.fuckgg@mail.ru', '7c4b408a33050981db4405fb5b360db5495ea6bc', 'amofuckgg');
    }

    public function test($leads, $contacts)
    {
        try {
            //add contacts
            $new_contacts = $this->service->addContacts($contacts);


            //adding to leads array contacts id
            foreach ($leads as $key => $lead) {

                foreach ($new_contacts as $new_contact) {
                    $leads[$key]['contacts_id'][] = $new_contact['id'];
                }
            }

            //add leads
            $new_leads = $this->service->addLeads($leads);

            $update_leads = [];
            $new_status = '123124';

            //update created leads
            if (isset($new_leads)) {

                foreach ($new_leads as $new_lead) {
                    $update_leads[] = [
                        'id' => $new_lead['id'],
                        'status_id' => $new_status
                    ];
                }

                $updated_leads = $this->service->updateLeads($update_leads);
            }

        } catch (Exception $exception) {
            echo $exception->getMessage();
        }

        print_r(['new contacts' => $new_contacts, 'new leads' => $new_leads, 'update leads' => $updated_leads]);

        return $leads;
    }
}

$example = new example();

$contacts = [
    //first contact
    2 => [
        'name' => 'test contact 4',

        //contact phone info
        'phones' => [

            [
                'phone' => '88005553535',
                'phone_enum' => '443615',
                'phone_field_id' => '313845',
            ],
            [
                'phone' => '88005551111',
                'phone_enum' => '443615',
                'phone_field_id' => '313845',
            ]
        ],

        //contact email info
        'emails' => [
            [
                'email' => 'test@bk.ru"',
                'email_enum' => '443627',
                'email_field_id' => '313847',
            ],
            [
                'email' => 'tes23t@bk.ru"',
                'email_enum' => '443627',
                'email_field_id' => '313847',
            ]
        ]
    ],

    //second contact
    4 => [
        'name' => 'test contact 5',
        'phones' => [
            [
                'phone' => '89995551212',
                'phone_enum' => '443615',
                'phone_field_id' => '313845',
            ]
        ],
    ],

    //third contact
    1 => [
        'name' => 'test contact 6'
    ]
];


$leads = [
    [
        'name' => 'buy potato',
        'status_id' => '12322',
        'responsible_user_id' => '28516282'
    ],

    [
        'name' => 'buy tomato',
        'status_id' => '12322',
        'responsible_user_id' => '28516282'
    ],
];

$new_leads = $example->test($leads, $contacts);

//find contact by email
$find_contact = $example->service->amoFindBy('contacts?query', 'test@bk.ru');
print_r($find_contact);

//find lead by name
$find_lead = $example->service->amoFindBy('leads?query', 'buy tomato');
print_r($find_lead);

//find lead by id
$find_lead = $example->service->amoFindBy('leads?id', array_shift($find_lead)['id']);
print_r($find_lead);

//find leads by ids
$leads_ids = null;
foreach ($new_leads as $new_lead) {
    $leads_ids = $new_lead['id'] . ",";
}

$find_lead = $example->service->amoFindBy('leads?id', $leads_ids);
print_r($find_lead);
